﻿using System;
using GoEpik.Packages.Interactions;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class SnapsStep : Step
{
    [SerializeField] private Transform _snapInteractionsHolder;
    [SerializeField] private ProgressCircle _progressCircle;

    private int _nOfInteractions;
    private int _counter;
    

    private void Start()
    {
        _snapInteractionsHolder.gameObject.SetActive(false);
    }

    protected override void OnStepStart()
    {
        _snapInteractionsHolder.gameObject.SetActive(true);
        var snaps = _snapInteractionsHolder.GetComponentsInChildren<SnapSlot>();
        _nOfInteractions = snaps.Length;
        _counter = 0;
        _progressCircle.SetProgress(_counter,snaps.Length);
        foreach (var snap in snaps)
        {
            snap.OnFinishInteraction.AddListener(CountInteraction);
            snap.BeginInteraction();
        }
    }

    protected override void OnStepUpdate()
    {
    }

    protected override void OnStepEnd()
    {
        _snapInteractionsHolder.gameObject.SetActive(false);
    }

    private void CountInteraction()
    {
        _progressCircle.SetProgress(++_counter);
        if (_counter >= _nOfInteractions) FinishStep();
    }
}
