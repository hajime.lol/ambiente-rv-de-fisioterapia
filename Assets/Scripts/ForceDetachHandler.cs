﻿using GoEpik.Packages.SteamVrExtensions;
using GoEpik.Packages.StepFlow;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ForceDetachHandler : MonoBehaviour
{
    private StepManager _stepManager;

    private void Start()
    {
        _stepManager = GetComponent<StepManager>();
        GetComponentsInChildren<GoEPIKPickable>().ForEach(_ =>
        {
            _stepManager.OnAllStepsFinished.AddListener( () =>
            {
                _.ForceDetach();
                Destroy(_.gameObject);
            });
        });
    }
}
