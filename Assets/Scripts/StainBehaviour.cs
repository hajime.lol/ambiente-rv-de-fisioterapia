﻿using System;
using System.Collections;
using System.Linq;
using GoEpik.Packages.OverlapCollider;
using UnityEngine;
using UnityEngine.Events;


[RequireComponent(typeof(BoxOverlapCollider))]
public class StainBehaviour : MonoBehaviour
{
    private BoxOverlapCollider Coll => _coll ?? (_coll = GetComponent<BoxOverlapCollider>());
    private BoxOverlapCollider _coll;

    private SpriteRenderer SprtRenderer => _sprtRenderer ?? (_sprtRenderer = GetComponent<SpriteRenderer>());
    private SpriteRenderer _sprtRenderer;


    [SerializeField] private float _normalized = 0;
    [SerializeField] private float _metersToComplete = 1;

    public UnityEvent OnFinish = new UnityEvent();
    
    private void Start()
    {
        Coll.OnOverlapEnter.AddListener(StartClean);
    }

    private void StartClean(Collider[] colls)
    {
        StopAllCoroutines();
        StartCoroutine(Cleaning());
    }

    private void Finished()
    {
        OnFinish.Invoke();
        Destroy(gameObject);
    }


    private IEnumerator Cleaning()
    {
        yield return null;
        var lastPos = Coll.StayingColliders.First().transform.position;

        var stainColor = SprtRenderer.color;

        while (Coll.StayingColliders.Length > 0)
        {
            var actual = Coll.StayingColliders.First().transform.position;
            var delta = (actual - lastPos).magnitude;
            lastPos = actual;

            _normalized += delta / _metersToComplete;
            
            stainColor.a = 1f - _normalized;
            SprtRenderer.color = stainColor;
            
            if (_normalized >= 1)
                Finished();

            yield return null;
        }
    }
}