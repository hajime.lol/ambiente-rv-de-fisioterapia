﻿using System.Collections;
using GoEpik.Packages.SteamVrExtensions;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class StickBehaviourV2 : MonoBehaviour
{
    [SerializeField] private float _angleTreshold = 15f;
    [SerializeField] private int _numberOfInteractions = 10;
    [SerializeField] private ProgressCircle _progressCircle;
    
    
    private int _counter;
    
    private GoEPIKPickable _goEpikPickable;
    private Transform _targetHand;
    private bool _isClosingAngle;
    private StickIndicator _indicator;

    public UnityEvent OnFinish = new UnityEvent();
    
    private void Start()
    {
        _angleTreshold = 90 - SettingsPanel.BastaoAngle;
        _indicator = GetComponentInChildren<StickIndicator>();
        _goEpikPickable = GetComponentInParent<GoEPIKPickable>();
        _goEpikPickable.OnAttach.AddListener(Attach);
        _goEpikPickable.OnDetach.AddListener(Detach);
        _indicator.gameObject.SetActive(false);
        _progressCircle.SetProgress(_counter,_numberOfInteractions);
    }

    private void Attach(Hand hand)
    {
        _targetHand = hand.otherHand.objectAttachmentPoint;
        StartCoroutine(UpdateRotation());
        _indicator.gameObject.SetActive(true);
        _indicator.ChangeDirection(_isClosingAngle);
    }

    private void Detach(Hand hand)
    {
        _indicator.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private IEnumerator UpdateRotation()
    {
        var lastPos = transform.position;
        while (true)
        {
            Transform t = transform;
            t.LookAt(_targetHand);

            float angle = Vector3.Angle(Vector3.up, t.forward);

            if ((_isClosingAngle && angle < _angleTreshold) ||
                (!_isClosingAngle && angle > 180 - _angleTreshold))
            {
                _isClosingAngle = !_isClosingAngle;
                _counter++;
                _progressCircle.SetProgress(_counter,_numberOfInteractions);
                _indicator.ChangeDirection(_isClosingAngle);
                if (_counter >= _numberOfInteractions) break;
            }
            yield return null;
        }
        OnFinish.Invoke();
    }
}