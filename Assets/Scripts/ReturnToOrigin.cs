﻿using System.Collections;
using GoEpik.Packages.SteamVrExtensions;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ReturnToOrigin : MonoBehaviour
{
    [SerializeField] private bool _isLocal = false;
    [SerializeField] private float _lerpDuration = 1;
    
    private GoEPIKPickable _goEpikPickable;

    private Vector3 _originalPosition;
    private Quaternion _originalRotation;
    
    private void Start()
    {
        var t = transform;
        _originalPosition = (_isLocal) ? t.localPosition : t.position;
        _originalRotation = (_isLocal) ? t.localRotation : t.rotation;
        
        _goEpikPickable = GetComponentInParent<GoEPIKPickable>();
        _goEpikPickable.OnDetach.AddListener(Return);
        _goEpikPickable.OnAttach.AddListener(_ => StopAllCoroutines());
    }

    protected virtual void Return(Hand arg0)
    {
        StopAllCoroutines();
        StartCoroutine(ReturnRoutine());
    }

    private IEnumerator ReturnRoutine()
    {
        var t = transform;
        var startingPos = (_isLocal) ? t.localPosition : t.position;
        var startingRot = (_isLocal) ? t.localRotation : t.rotation;

        var interpolation = 0f;

        while (interpolation<1)
        {
            interpolation += Time.deltaTime / _lerpDuration;
            var desiredPos = Vector3.Lerp(startingPos, _originalPosition, interpolation);
            var desiredRot = Quaternion.Lerp(startingRot, _originalRotation, interpolation);
            if (_isLocal)
            {
                t.localPosition = desiredPos;
                t.localRotation = desiredRot;
            }
            else
            {
                t.position = desiredPos;
                t.rotation = desiredRot;
            }
            yield return null;
        }
    }
}
