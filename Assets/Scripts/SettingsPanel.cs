﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : MonoBehaviour
{
    private (Slider slider,InputField input) _alturadamaca;
    public static Action<float> OnAlturaDaMacaChanged;
    public static float AlturaDaMaca
    {
        get => PlayerPrefs.GetFloat("altura_da_maca", 0.88f);
        private set
        {
            var value2 = value/100;
            OnAlturaDaMacaChanged.Invoke(value2);
            PlayerPrefs.SetFloat("altura_da_maca", value2);
        }
    }

    private (Slider slider,InputField input) _panelas;
    public static int Panelas
    {
        get => PlayerPrefs.GetInt("panelas", 4);
        private set => PlayerPrefs.SetInt("panelas", value);
    }
    
    
    private (Slider slider,InputField input) _panelasRot;
    public static int PanelasRot
    {
        get => PlayerPrefs.GetInt("panelas_rot", 10);
        private set => PlayerPrefs.SetInt("panelas_rot", value);
    }
    
    private (Slider slider,InputField input) _bastaoAngle;
    public static int BastaoAngle
    {
        get => PlayerPrefs.GetInt("bastao_angle", 75);
        private set => PlayerPrefs.SetInt("bastao_angle", value);
    }
    
    private void Awake()
    {
        var sliders = GetComponentsInChildren<Slider>();
        var inputs = GetComponentsInChildren<InputField>();
        _alturadamaca.slider = sliders[0];
        _alturadamaca.input = inputs[0];
        _panelas.slider = sliders[1];
        _panelas.input = inputs[1];
        _panelasRot.slider = sliders[2];
        _panelasRot.input = inputs[2];
        _bastaoAngle.slider = sliders[3];
        _bastaoAngle.input = inputs[3];
        gameObject.SetActive(false);
    }
    
    private void Start()
    {
        _alturadamaca.slider.onValueChanged.AddListener(_ =>
        {
            _alturadamaca.input.text = _.ToString() + " cm";
            AlturaDaMaca = _;
        });
        _alturadamaca.slider.value = AlturaDaMaca*100;
        _alturadamaca.input.text = (_alturadamaca.slider.value = AlturaDaMaca*100).ToString() + " cm";
        
        _panelas.slider.onValueChanged.AddListener(_ =>
        {
            _panelas.input.text = _.ToString() + " panelas";
            Panelas = (int) _;
        });
        _panelas.slider.value = Panelas;
        _panelas.input.text = (_panelas.slider.value = Panelas).ToString() + " panelas";
        
        _panelasRot.slider.onValueChanged.AddListener(_ =>
        {
            _panelasRot.input.text = _.ToString() + " voltas";
            PanelasRot = (int) _;
        });
        _panelasRot.slider.value = PanelasRot;
        _panelasRot.input.text = (_panelasRot.slider.value = PanelasRot).ToString() + " voltas";
        
        _bastaoAngle.slider.onValueChanged.AddListener(_ =>
        {
            _bastaoAngle.input.text = _.ToString() + "°";
            BastaoAngle = (int) _;
        });
        _bastaoAngle.slider.value = BastaoAngle;
        _bastaoAngle.input.text = (_bastaoAngle.slider.value = BastaoAngle).ToString() + "°";
    }
}
