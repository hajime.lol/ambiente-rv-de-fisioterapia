﻿using UnityEngine;
using Valve.VR.InteractionSystem;

public class Teleporter : MonoBehaviour
{
    #region Singleton Stuff _instance
    
    private static Teleporter _instance;

    private void Awake()
    {
        _instance = this;
    }

    private void OnDestroy()
    {
        if (_instance = this) _instance = null;
    }
    #endregion

    private Transform _chaperone;
    private Transform Chaperone => _chaperone ?? (_chaperone = Player.instance.transform);
    
    private Transform _head;
    private Transform Head => _head ?? (_head = Player.instance.hmdTransform);

    public static void MoveTo(Vector3 position)
    {
        var headPosition = _instance.Head.localPosition;
        headPosition = _instance.Chaperone.rotation * headPosition;
        headPosition.y = 0;
        _instance.Chaperone.position = position - headPosition;
    }

    public static void LookTo(Vector3 direction)
    {
        var headForward = _instance.Head.forward;
        headForward.y = 0;

        var desiredForward = direction;
        desiredForward.y = 0;

        var yRotation = Vector3.SignedAngle(desiredForward, headForward,Vector3.down);
        
        _instance.Chaperone.RotateAround(_instance.Head.position,Vector3.up,yRotation);
    }

    public static void GoTo(Vector3 position, Vector3 forward)
    {
        LookTo(forward);
        MoveTo(position);
    }
}