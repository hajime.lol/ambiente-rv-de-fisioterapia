﻿using UnityEngine;

public class CanvasProgressCircle : MonoBehaviour
{
    public static ProgressCircle ProgressCircle => _instance._progressCircle;
    
    private static CanvasProgressCircle _instance;

    private ProgressCircle _progressCircle;
    
    private void Awake()
    {
        _instance = this;
        _progressCircle = GetComponent<ProgressCircle>();
        _progressCircle._isCanvas = true;
        gameObject.SetActive(false);
    }

    public static void Enabled(bool value)
    {
        _instance.gameObject.SetActive(value);
    }
}
