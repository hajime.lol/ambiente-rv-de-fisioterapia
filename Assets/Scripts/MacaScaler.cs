﻿using System;
using UnityEngine;

public class MacaScaler : MonoBehaviour
{
    private void Awake()
    {
        SettingsPanel.OnAlturaDaMacaChanged += SetHeight;
    }

    private void Start()
    {
        SetHeight(SettingsPanel.AlturaDaMaca);
    }

    private void SetHeight(float height)
    {
        transform.localScale = new Vector3(1, height, 1);
    }
}
