﻿using UnityEngine;

public class HeightOnMacaLevel : MonoBehaviour
{
    private void Awake()
    {
        var pos = transform.localPosition;
        pos.y = SettingsPanel.AlturaDaMaca;
        transform.localPosition = pos;
    }
}
