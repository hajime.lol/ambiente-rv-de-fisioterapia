﻿using System.Collections;
using System.Collections.Generic;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class BastaoStep : Step
{
    [SerializeField] private StickBehaviourV2 _stick;
    
    protected override void OnStepStart()
    {
        _stick.OnFinish.AddListener(FinishStep);
    }

    protected override void OnStepUpdate()
    {
    }

    protected override void OnStepEnd()
    {
    }
}
