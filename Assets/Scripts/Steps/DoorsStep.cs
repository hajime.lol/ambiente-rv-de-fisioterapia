﻿using System;
using System.Collections;
using GoEpik.Packages.Interactions;
using GoEpik.Packages.SteamVrExtensions;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class DoorsStep : Step
{
    [SerializeField] private GameObject _toOpen;
    [SerializeField] private GameObject _toClose;

    [SerializeField] private int _nOfInteractions = 10;
    //[SerializeField] private ProgressCircle _progressCircle;

    private int _counter;

    private RotationInteraction _openedDoor;
    private RotationInteraction _closedDoor;

    protected override void OnStepStart()
    {
        //_progressCircle.SetProgress(0,_nOfInteractions);
        Closed();
    }

    protected override void OnStepUpdate()
    {
    }

    protected override void OnStepEnd()
    {
    }

    private void Opened()
    {
        if (Iterate())
        {
            FinishStep();
            return;
        }
        _closedDoor = Instantiate(_toClose, transform).GetComponent<RotationInteraction>();
        _closedDoor.OnFinishInteraction.AddListener(() =>
        {
            Closed();
            _closedDoor.Handle.GetComponent<GoEPIKPickable>().ForceDetach();
            Destroy(_closedDoor.Handle.gameObject);
            Destroy(_closedDoor.gameObject);
        });
        StartCoroutine(DelayedBeginInteraction(_closedDoor));
    }

    private void Closed()
    {
        if (Iterate())
        {
            FinishStep();
            return;
        }

        _openedDoor = Instantiate(_toOpen, transform).GetComponent<RotationInteraction>();
        _openedDoor.OnFinishInteraction.AddListener(() =>
        {
            Opened();
            _openedDoor.Handle.GetComponent<GoEPIKPickable>().ForceDetach();
            Destroy(_openedDoor.Handle.gameObject);
            Destroy(_openedDoor.gameObject);
        });
        StartCoroutine(DelayedBeginInteraction(_openedDoor));
    }

    private bool Iterate()
    {
        //_progressCircle.SetProgress(++_counter);
        return _counter >= _nOfInteractions;
    }

    private IEnumerator DelayedBeginInteraction(RotationInteraction interaction)
    {
        yield return null;
        interaction.BeginInteraction();
    }
}
