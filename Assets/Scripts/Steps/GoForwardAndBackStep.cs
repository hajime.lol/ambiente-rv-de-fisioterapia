﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GoEpik.Packages.StepFlow;
using UnityEngine;
using UnityEngine.Serialization;

public class GoForwardAndBackStep : Step
{
    [SerializeField] private GoForwardAndBack _goForwardAndBack;
    [SerializeField] private int _interactions;
    [SerializeField] private ProgressCircle _progressCircle;
    
    private int _counter = 0;
    
    protected override void OnStepStart()
    {
        _counter = 0;
        _goForwardAndBack.OnInteraction.AddListener(Interact);
        _progressCircle.SetProgress(_counter,_interactions);
    }

    protected override void OnStepUpdate()
    {
    }

    protected override void OnStepEnd()
    {
        _goForwardAndBack.OnInteraction.RemoveListener(Interact);
    }
    
    private void Interact()
    {
        _counter++;
        _progressCircle.SetProgress(_counter,_interactions);
        if (_counter >= _interactions) FinishStep();
    }
}
