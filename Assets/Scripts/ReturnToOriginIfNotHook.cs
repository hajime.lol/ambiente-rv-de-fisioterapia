﻿using System.Linq;
using GoEpik.Packages.OverlapCollider;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ReturnToOriginIfNotHook : ReturnToOrigin
{
    private OverlapCollider _coll;
    protected override void Return(Hand arg0)
    {
        if (_coll.StayingColliders.Any(_ =>_.CompareTag("Hook"))) return;
        base.Return(arg0);
    }
}
