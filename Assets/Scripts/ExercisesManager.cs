﻿using System;
using GoEpik.Packages.CustomAttributes;
using GoEpik.Packages.StepFlow;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ExercisesManager : MonoBehaviour
{
    [SerializeField] private Transform _canvasContent;
    [SerializeField] private GameObject _weekSeparator;
    [SerializeField] private GameObject _interactionButton;   
    
    [System.Serializable]
    private struct Group
    {
        public string GroupName;
        public GameObject[] Exercises;
    }

    [SerializeField] private Group[] _groups;

    [Header("Debug")]
    [SerializeField] private int _weekIndex;
    [SerializeField] private int _exerciseIndex;

    private StepManager _actualStepManager;
    
    private void Start()
    {
        for (int w = 0;w<_groups.Length;w++)
        {
            InstantiateWeek(_groups[w].GroupName);
            foreach (var exercise in _groups[w].Exercises)
            {
                InstantiateButton(exercise);
            }
        }
    }

    private void InstantiateWeek(string label)
    {
        var text = Instantiate(_weekSeparator, _canvasContent).GetComponentInChildren<Text>();
        text.text = label;
    }

    private void InstantiateButton(GameObject exercise)
    {
        var btn = Instantiate(_interactionButton, _canvasContent).GetComponent<Button>();
        btn.GetComponentInChildren<Text>().text = exercise.name;
        btn.onClick.AddListener( () =>
        {
            StartExercise(exercise);
        });
    }

    public void StartExercise(GameObject exercise)
    {
        if (_actualStepManager != null)
        {
            _actualStepManager.ForceStopManager(true);
        }
        _actualStepManager = Instantiate(exercise).GetComponent<StepManager>();
        
    }
    
    [Button]
    public void StartExercise()
    {
        var stepManager = Instantiate(_groups[_weekIndex].Exercises[_exerciseIndex]).GetComponent<StepManager>();
        stepManager.OnAllStepsFinished.AddListener(() => Destroy(stepManager.gameObject));
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }
    
}
