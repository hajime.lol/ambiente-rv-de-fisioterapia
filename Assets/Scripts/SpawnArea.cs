﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnArea : MonoBehaviour
{
    [SerializeField] private Bounds _bounds;

    public Vector3 GetPosition()
    {
        return transform.position + new Vector3(Random.Range(_bounds.min.x,_bounds.max.x),Random.Range(_bounds.min.y,_bounds.max.y),Random.Range(_bounds.min.z,_bounds.max.z));
    }
    
    private void OnDrawGizmosSelected()
    {
        var defaultMatrix = Gizmos.matrix;
        var matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.matrix = matrix;
        Gizmos.DrawCube(_bounds.center,_bounds.size);

        Gizmos.matrix = defaultMatrix;
    }
}
