﻿using UnityEngine;

public class StickIndicator : MonoBehaviour
{
    private Transform _childArrow;
    private void Start()
    {
        _childArrow = transform.GetChild(0);
    }


    public void ChangeDirection(bool value)
    {
        _childArrow.forward = value ? transform.forward : -transform.forward;
    }
}
