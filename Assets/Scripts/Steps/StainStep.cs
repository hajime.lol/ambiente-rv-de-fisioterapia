﻿using System.Collections;
using System.Collections.Generic;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class StainStep : Step
{
    [SerializeField] private int _numberOfStains = 4;
    [SerializeField] private SpawnArea _spawnArea;
    [SerializeField] private GameObject _stainPrefab;
    [SerializeField] private ProgressCircle _progressCircle;

    private StainBehaviour _stain;
    private int _counter;
    
    protected override void OnStepStart()
    {
        _counter = -1;
        NextStain();
    }

    protected override void OnStepUpdate()
    {
        
    }

    protected override void OnStepEnd()
    {
        
    }

    private void NextStain()
    {
        _counter++;
        _progressCircle.SetProgress(_counter, _numberOfStains);
        if(_counter >= _numberOfStains) FinishStep();
        else SpawnStain();
    }
    
    private void SpawnStain()
    {
        _stain = Instantiate(_stainPrefab,_spawnArea.GetPosition(),Quaternion.Euler(90,0,0),_spawnArea.transform).GetComponent<StainBehaviour>();
        _stain.OnFinish.AddListener(NextStain);
    }
}
