﻿using System;
using GoEpik.Packages.OverlapCollider;
using UnityEngine;

public class HandHoverValidator : MonoBehaviour
{
    [SerializeField] private Color _trueColor = Color.green;
    [SerializeField] private Color _falseColor = Color.red;
    
    private bool _isSomeHandHere;
    public bool HandIn => _isSomeHandHere;

    private OverlapCollider _overlapRegion;
    private Material _silhoueteMat;

    private void Start()
    {
        _silhoueteMat = GetComponentInChildren<MeshRenderer>().material;
        _overlapRegion = GetComponent<OverlapCollider>();
        _overlapRegion.OnOverlapEnter.AddListener(OnEnter);
        _overlapRegion.OnOverlapExit.AddListener(OnExit);
        ChangeColor(_isSomeHandHere);
    }

    private void OnEnter(Collider[] arg0)
    {
        ChangeColor(_isSomeHandHere = true);
    }

    private void OnExit(Collider[] arg0)
    {
        if (_overlapRegion.StayingColliders.Length > 0) return;

        ChangeColor(_isSomeHandHere = false);
    }

    private void ChangeColor(bool value)
    {
        _silhoueteMat.SetColor("g_vOutlineColor",value?_trueColor:_falseColor);
    }
}
