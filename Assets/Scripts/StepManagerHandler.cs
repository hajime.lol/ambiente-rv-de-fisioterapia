﻿using System.Collections;
using System.Collections.Generic;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class StepManagerHandler : MonoBehaviour
{
    void Start()
    {
        GetComponent<StepManager>().OnAllStepsFinished.AddListener(KillMe);
    }

    private void KillMe()
    {
        StartCoroutine(Death());
    }

    private IEnumerator Death()
    {
        yield return null;
        Destroy(gameObject);
    }
}
