﻿using System.Collections;
using System.Collections.Generic;
using GoEpik.Packages.Fader;
using GoEpik.Packages.StepFlow;
using UnityEngine;

public class SetPlayerPositionStep : Step
{
    [SerializeField] private Transform _target;
    protected override void OnStepStart()
    {
        StartCoroutine(Routine());
    }

    protected override void OnStepUpdate()
    {
        
    }

    protected override void OnStepEnd()
    {
    }

    private IEnumerator Routine()
    {
        Fade.To(Color.black,0.6f);
        yield return new WaitForSeconds(0.6f);
        Teleporter.GoTo(_target.position,_target.forward);
        Fade.To(Color.clear,0.6f);
        FinishStep();
    }
}
