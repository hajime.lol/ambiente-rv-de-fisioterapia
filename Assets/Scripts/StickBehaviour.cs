﻿using System.Collections;
using GoEpik.Packages.SteamVrExtensions;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class StickBehaviour : MonoBehaviour
{
    [SerializeField] private float _requiredMovement = 0.15f;
    [SerializeField] private int _numberOfInteractions = 10;
    private int _counter;
    
    private GoEPIKPickable _goEpikPickable;
    private Transform _targetHand;
    private bool _isWellPositioned;
    private bool _isInFavor;
    private float _aculatedDelta;
    private StickIndicator _indicator;

    public UnityEvent OnFinish = new UnityEvent();
    
    [SerializeField] private float _expectedAngle = 30;
    private float TreshHold => Mathf.Sin(_expectedAngle*Mathf.Deg2Rad);
    private void Start()
    {
        _indicator = GetComponentInChildren<StickIndicator>();
        _goEpikPickable = GetComponentInParent<GoEPIKPickable>();
        _goEpikPickable.OnAttach.AddListener(Attach);
        _goEpikPickable.OnDetach.AddListener(Detach);
        _indicator.gameObject.SetActive(false);
    }

    private void Attach(Hand hand)
    {
        _targetHand = hand.otherHand.objectAttachmentPoint;
        StartCoroutine(UpdateRotation());
        _indicator.gameObject.SetActive(true);
    }

    private void Detach(Hand hand)
    {
        _indicator.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private IEnumerator UpdateRotation()
    {
        var lastPos = transform.position;
        while (true)
        {
            transform.LookAt(_targetHand);
            _isWellPositioned = Mathf.Abs(transform.forward.y) <= TreshHold;

            if (_isWellPositioned)
            {
                var actualPosition = transform.position;
                Iterate(actualPosition - lastPos);
                lastPos = actualPosition;
            }
            yield return null;
        }
    }

    private void Iterate(Vector3 delta)
    {
        var desiredDirection = transform.forward * (_isInFavor ? 1 : -1);
        if (Vector3.Angle(desiredDirection, delta) < 15)
        {
            _aculatedDelta += delta.magnitude;
            if (_aculatedDelta >= _requiredMovement)
            {
                _isInFavor = !_isInFavor;
                _counter++;
                _indicator.ChangeDirection(_isInFavor);
                _aculatedDelta = 0;
                if (_counter >= _numberOfInteractions) OnFinish.Invoke();
            }
        }

    }
    
    
}