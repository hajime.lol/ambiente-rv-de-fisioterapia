﻿using System;
using GoEpik.Packages.Interactions;
using GoEpik.Packages.SteamVrExtensions;
using GoEpik.Packages.StepFlow;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CaldeiraoStep : Step
{
    [SerializeField] private RotationInteraction[] _rotationInteraction;
    [SerializeField] private float _numberOfTurns = 10;
    [SerializeField] private ProgressCircle _progressCircle;
    private int index;

    protected override void OnStepStart()
    {
        _rotationInteraction.ForEach(_ => _.gameObject.SetActive(false));
        Array.Resize(ref _rotationInteraction, SettingsPanel.Panelas);
        _rotationInteraction.ForEach(_ => _.gameObject.SetActive(true));
        _numberOfTurns = SettingsPanel.PanelasRot;
        
        index = -1;
        _rotationInteraction.ForEach(_ =>
        {
            _.Manipulated.gameObject.SetActive(false);
            _.Max = _.Min = 0;
            _.EndInteraction();
            _.Min = -(_.Max = 360 * _numberOfTurns);
        });
        NextCaldron();
    }

    protected override void OnStepUpdate()
    {
        if (_rotationInteraction[index].NormalizedValue <= 0 || _rotationInteraction[index].NormalizedValue >= 1)
        {
            FinishCaldron(_rotationInteraction[index]);
        }
    }

    protected override void OnStepEnd()
    {
    }

    private void NextCaldron()
    {
        _progressCircle.SetProgress(++index,_rotationInteraction.Length);
        
        if (index >= _rotationInteraction.Length)
        {
            FinishStep();
            return;
        }
        _rotationInteraction[index].BeginInteraction();
        _rotationInteraction[index].Manipulated.gameObject.SetActive(true);
    }

    private void FinishCaldron(RotationInteraction caldron)
    {
        caldron.Handle.GetComponent<GoEPIKPickable>().ForceDetach();
        caldron.EndInteraction();
        caldron.Manipulated.gameObject.SetActive(false);
        NextCaldron();
    }
}
