# Ambiente RV de Fisioterapia

Avaliar a eficácia da fisioterapia via RV, no ganho de amplitude de movimento em
pacientes com síndrome do ombro doloroso em comparação com fisioterapia
tradicional.