﻿using System;
using GoEpik.Packages.OverlapCollider;
using UnityEngine;
using UnityEngine.Events;

public class GoForwardAndBack : MonoBehaviour
{
    public UnityEvent OnInteraction = new UnityEvent();
    [SerializeField] private float _distance;
    [SerializeField] private HandHoverValidator _handHoverValidator;
    private GameObject _front;
    private GameObject _back;

    private void Start()
    {
        _front = transform.GetChild(0).gameObject;
        _back = transform.GetChild(1).gameObject;

        _front.transform.localPosition+= Vector3.forward*_distance;
        _back.transform.localPosition+= Vector3.back*_distance;
        
        _back.gameObject.SetActive(false);
        
        var frontColl = _front.GetComponent<BoxOverlapCollider>();
        var backColl = _back.GetComponent<BoxOverlapCollider>();

        frontColl.OnOverlapStay.AddListener(_ => NextInteraction());
        backColl.OnOverlapStay.AddListener(_ => NextInteraction());
        
    }

    private void NextInteraction()
    {
        if (_handHoverValidator != null && !_handHoverValidator.HandIn) return;
        
        _front.SetActive(!_front.activeSelf);
        _back.SetActive(!_back.activeSelf);
        OnInteraction.Invoke();
    }
}