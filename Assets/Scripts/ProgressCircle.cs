﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressCircle : MonoBehaviour
{
    [SerializeField] private Gradient _colorProgression;
    public bool _isCanvas = false;
    
    private Image _fillImage;
    private Image FillImage => _fillImage ?? (_fillImage = GetComponentInChildren<Image>());
    
    private float _maxValue = 1;

    // Start is called before the first frame update
    void Start()
    {
        FillImage.fillAmount = 0;
        CanvasProgressCircle.Enabled(true);
    }

    private void OnDestroy()
    {
        CanvasProgressCircle.Enabled(false);
    }

    public void SetProgress(float progressValue, float maxValue)
    {
        _maxValue = maxValue;
        float value = (progressValue + 1) / (maxValue + 1);
        FillImage.fillAmount = value;
        FillImage.color = _colorProgression.Evaluate(value);
        if (!_isCanvas) CanvasProgressCircle.ProgressCircle.SetProgress(progressValue, maxValue);
    }
    
    public void SetProgress(float progressValue)
    {
        float value = (progressValue + 1) / (_maxValue + 1);
        FillImage.fillAmount = value;
        FillImage.color = _colorProgression.Evaluate(value);
        if (!_isCanvas) CanvasProgressCircle.ProgressCircle.SetProgress(progressValue);
    }
    
}
