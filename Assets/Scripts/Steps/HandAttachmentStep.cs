﻿using System.Collections;
using System.Collections.Generic;
using GoEpik.Packages.StepFlow;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class HandAttachmentStep : Step
{
    [SerializeField] private GameObject _spawnable;

    protected override void OnStepStart()
    {
        var hands = Player.instance.hands;
        foreach (var hand in hands)
        {
            var attachedObject = hand.currentAttachedObject;
            
            if(attachedObject != null)
            {
                hand.DetachObject(attachedObject);
                Destroy(attachedObject);
            }

            if (_spawnable != null)
            {
                var changed = Instantiate(_spawnable).gameObject;

                hand.AttachObject(changed, GrabTypes.None,
                    Hand.AttachmentFlags.ParentToHand | Hand.AttachmentFlags.SnapOnAttach);
            }
        }
        FinishStep();
    }

    protected override void OnStepUpdate() {}

    protected override void OnStepEnd() {}
}

